'use strict';
var app = angular.module('myShop', []);

app.controller('RegisterUser', function($rootScope,$scope) {
	$rootScope.listUser = [];

	$scope.regissubmit = function() {
        $rootScope.myModalShow = false;

		if ($scope.name && $scope.email && $scope.telephone && $scope.pass) {
            $rootScope.myRegisterShow = true;
			$scope.listUser.push(this.name, this.email, this.telephone, this.pass);
			$scope.name = $scope.email = $scope.telephone = $scope.pass = '';
		}
	};
});

app.controller('CheckingLogin', function($scope, $http) {
    // create a blank object to hold our form information
	// $scope will allow this to pass between controller and view
	$scope.formData = {};
	$scope.errorName = 'Enter Username';
	$scope.errorPass = 'Enter Password';
	// process the form
	$scope.processForm = function() {
		$http({
	        method  : 'POST',
	        url     : 'process.php',
	        data    : $.param($scope.formData),  // pass in data as strings
	        headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
	    })
	        .success(function(data) {
	            if (!data.success) {
	            	// if not successful, bind errors to error variables
	                $scope.errorName = data.errors.name;
	                $scope.errorNameShow = data.errors.name;
	                $scope.errorPass = data.errors.pass;
	                $scope.errorPassShow = data.errors.pass;
	            } else {
	            	// if successful, bind success message to message
	                $scope.message = data.message;
                    $scope.errorName = '';
                    $scope.errorNameShow = '';
	                $scope.errorPass = '';
	                $scope.errorPassShow = '';
	                $scope.formData = '';
	            }
	        });
	};
});

// var userlist = [
// 	{ name: "tuan", pass: 123 },
// 	{ name: "tuanevt", pass: 123 }
// ];