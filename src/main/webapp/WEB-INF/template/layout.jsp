<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en" ng-app="myShop">
    <head>
        <meta charset="UTF-8"/>
        <title><tiles:insertAttribute name="title" /></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/resources/css/bootstrap.min.css">
        <link rel="stylesheet" href="/resources/css/style-main.css">
        <script src="/resources/js/angular.min.js"></script>
        <script src="/resources/js/app.js"></script>
        <script src="/resources/js/jquery-2.1.0.min.js"></script>
        <script src="/resources/js/bootstrap.min.js"></script>
    </head>

    <body>
        <div class="container">
        
        	<!-- Header -->
            <tiles:insertAttribute name="header" />
            
            <!-- Show main product + menu-->
            <div class="row thumbnail" id="gallery">
            	<tiles:insertAttribute name="content" />
            	<tiles:insertAttribute name="menu" />
            </div>
            
            <!-- Footer -->
            <tiles:insertAttribute name="footer" />

        </div>
        <script type="text/javascript" src="/resources/js/slide.js"></script>
    </body>
</html>