package com.tuanna45.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.tuanna45.dao.impl.UserDaoImpl;
import com.tuanna45.model.LoginInfo;
import com.tuanna45.model.RegisterInfo;

@Controller
public class FormController {
	
	@Autowired
	UserDaoImpl logic;
	
	 @RequestMapping("/about.html")
	 public String callAboutPage() {
		return "about";
	  }
	
	 /**
	   * This creates a new loginInfo object for the empty form and stuffs it into
	   * the model
	   */
	  @ModelAttribute("loginInfo")
	  public LoginInfo initLoginInfo() {
	    LoginInfo loginInfo = new LoginInfo();
	    return loginInfo;
	  }
	  
	  /**
	   * This creates a new registerInfo object for the empty form and stuffs it into
	   * the model
	   */
	  @ModelAttribute("registerInfo")
	  public RegisterInfo initRegisterInfo() {
		  RegisterInfo registerInfo = new RegisterInfo();
	    return registerInfo;
	  }	  
	
	 /**
	  * Create the initial blank form
	  */
	@RequestMapping(value = "/home.html", method = RequestMethod.GET)
	  public String createForm() {
		initLoginInfo();
		initRegisterInfo();
	    return "home";
	  }

	@RequestMapping(value = "/checkLogin.html", method = RequestMethod.POST)
	public String checkLogin(@ModelAttribute("loginInfo") LoginInfo logininfo, ModelMap model) {
		model.addAttribute("nameUser", logininfo.getUsername());
		model.addAttribute("passUser", logininfo.getPasswd());
		return "result";
	}
	
	@RequestMapping(value = "/checkRegister.html", method = RequestMethod.POST)
	public String checkLogin(@ModelAttribute("registerInfo") RegisterInfo registerInfo, ModelMap model) {
//		model.addAttribute("r_nameUser", registerInfo.getRuser());
//		model.addAttribute("r_passUser", registerInfo.getRpasswd());
//		model.addAttribute("r_mailUser", registerInfo.getRemail());
//		model.addAttribute("r_phoneUser", registerInfo.getRphone());
		model.addAttribute("message", "Registration Successful !!!");
		logic.insertUser(registerInfo);
		return "success";
	}
	
	

}
