package com.tuanna45.model;

public class LoginInfo {
	
	// Login infomations
	private String username;
	private String passwd;
	
	/**
	 * setUsername
	 * @param username
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	
	/**
	 * setPasswd
	 * @param passwd
	 */
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	
	/**
	 * getUsername
	 * @return username
	 */
	public String getUsername() {
		return username;
	}
	
	/**
	 * getPasswd
	 * @return passwd
	 */
	public String getPasswd() {
		return passwd;
	}
}
