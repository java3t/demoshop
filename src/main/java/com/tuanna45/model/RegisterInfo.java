package com.tuanna45.model;

public class RegisterInfo {
	
	// Register infomations
	private String ruser;
	private String remail;
	private Integer rphone;
	private String rpasswd;
	
	/**
	 * @return the ruser
	 */
	public String getRuser() {
		return ruser;
	}
	/**
	 * @param ruser the ruser to set
	 */
	public void setRuser(String ruser) {
		this.ruser = ruser;
	}
	/**
	 * @return the remail
	 */
	public String getRemail() {
		return remail;
	}
	/**
	 * @param remail the remail to set
	 */
	public void setRemail(String remail) {
		this.remail = remail;
	}
	/**
	 * @return the rphone
	 */
	public Integer getRphone() {
		return rphone;
	}
	/**
	 * @param rphone the rphone to set
	 */
	public void setRphone(Integer rphone) {
		this.rphone = rphone;
	}
	/**
	 * @return the rpasswd
	 */
	public String getRpasswd() {
		return rpasswd;
	}
	/**
	 * @param rpasswd the rpasswd to set
	 */
	public void setRpasswd(String rpasswd) {
		this.rpasswd = rpasswd;
	}
	
	
}
