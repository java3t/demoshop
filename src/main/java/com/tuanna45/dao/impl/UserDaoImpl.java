package com.tuanna45.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tuanna45.dao.IUserDao;
import com.tuanna45.model.RegisterInfo;

@Component
public class UserDaoImpl implements IUserDao {

	@Autowired
	IUserDao dao;
	
	public IUserDao getDao() {
		return dao;
	}
	
	public void setDao(IUserDao dao) {
		this.dao = dao;
	}

	@Override
	public int insertUser(RegisterInfo registerinfo) {
		return this.dao.insertUser(registerinfo);
	}
	
}
