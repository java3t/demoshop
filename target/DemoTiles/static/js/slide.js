$('#myModal').modal('show');

/// Solution auto slide 3

var speed = 15;
var tab = document.getElementById("slideB");
var tab1 = document.getElementById("inslideB1");
var tab2 = document.getElementById("inslideB2");

tab2.innerHTML = tab1.innerHTML;
// Command above you can use command below similar mean Using Jquery
// $("#inslideB2").append($("#inslideB1").html());


// Kiểm tra độ rộng của tab2 là bản sao của tab1 - vị trí hiện tại của thanh trượt ngang phải luôn nhỏ hơn = 0
// Nếu nó lớn hơn nghĩa là ta đã kéo thanh trượt ngang đến cuối cùng.
// Ngược lại, nếu ta chưa hề kéo thanh trượt ngang thì ta sẽ thực hiện kéo nó dịch lên 1 đơn vị.

function Marquee() {
    if (tab2.offsetWidth - tab.scrollLeft <= 0)
        tab.scrollLeft -= tab1.offsetWidth
    else {tab.scrollLeft++;}
}

var MyMar = setInterval(Marquee, speed);

tab.onmouseover = function() { 
    clearInterval(MyMar); 
};

tab.onmouseout = function() { 
    MyMar = setInterval(Marquee, speed);
};
