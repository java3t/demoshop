<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
    <head>
        <title><tiles:insertAttribute name="title" /></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="static/css/bootstrap.min.css">
        <link rel="stylesheet" href="static/css/style-main.css">
         <script src="static/js/jquery-2.1.0.min.js"></script>
         <script src="static/js/bootstrap.min.js"></script>
        <script src="static/js/triggermodal.js"></script>
    </head>

    <body>
        <div class="container">
        
        	<!-- Header -->
            <tiles:insertAttribute name="header" />
            
            <!-- Show main product + menu-->
            <div class="row thumbnail" id="gallery">
            	<tiles:insertAttribute name="content" />
            	<tiles:insertAttribute name="menu" />
            </div>
            
            <!-- Footer -->
            <tiles:insertAttribute name="footer" />

        </div>
        <script type="text/javascript" src="static/js/slide.js"></script>
    </body>
</html>