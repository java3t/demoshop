<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

			<!-- Head bar -->
            <div class="row" id="navi-bar">
                <div class="col-sm-10">
                    <ul class="nav navbar-nav">
                        <li><a href="home.html" class="a-main">Trang chủ</a></li>
                         <!--Tạo menu con San Pham-->
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Sản phẩm <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href>Sản phẩm 1</a></li>
                                <li><a href>Sản phẩm 2</a></li>
                                <li><a href>Sản phẩm 3</a></li>
                            </ul>
                        </li>
                        <li><a href="about.html" class="a-main">Giới thiệu</a></li>
                        <!--Tạo menu con-->
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Tin tức <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href>Tin tức 1</a></li>
                                <li><a href>Tin tức 2</a></li>
                                <li><a href>Tin tức 3</a></li>
                                <div class="divider"></div>
                                <li><a href>Tin tức mới</a></li>
                            </ul>
                        </li>
                        <li><a href class="a-main">Liên hệ</a></li>
                    </ul>
                </div>
                <div class="col-sm-2" id="navi-bar2">
                    <!--<form class="form-inline">-->
                    <!--    <div class="input-group">-->
                    <!--        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>-->
                    <!--        <input type="text" class="form-control" name="Nhap Ten" placeholder="Enter your username"/>-->
                    <!--    </div>-->
                    <!--    <div class="input-group">-->
                    <!--        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>-->
                    <!--        <input type="password" class="form-control" placeholder="Enter your password"/>-->
                    <!--    </div>-->
                    <!--        <input type="submit" class="btn btn-success" value="Login">-->
                    <!--        <input type="submit" class="btn btn-danger" value="Register">-->
                    <!--</form>-->
                    <a href="login.html"><input type="button" class="btn btn-success" value="Login" data-toggle="modal" data-target="#myModal"></a> 
                    <!-- <input type="button" class="btn btn-danger" value="Register" data-toggle="modal"  data-target="#myModal"> -->
                
					<!--Login Form Start-->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                    <h4 class="modal-title" id="myLargeModalLabel" style="color: white;">WELCOME TO FASHION TV</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-8" style="border-right: 1px dotted #C2C2C2;padding-right: 30px;">
                                            
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs">
                                                <li class="active" id="login_active"><a href="#Login" data-toggle="tab">Login</a></li>
                                                <li><a href="#Registration" data-toggle="tab">Registration</a></li>
                                            </ul>
                                            
                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                            
                                                <!-- Form Login -->
                                                <div class="tab-pane active" id="Login">
                                                    <form:form role="form" class="form-horizontal" action="checkLogin.html" method="POST" modelAttribute="loginInfo">
                                                        
                                                        <%-- <div id="messages" class="well">${message }</div> --%>
                                                        
														<div class="form-group">
															<div class="input-group">
															     <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
															     <div class="col-sm-10">
															     	<form:input type="text" path="username" class="form-control" id="name" placeholder="Enter Username" />
															     </div>                                                              
															</div>
															
															<div class="col-sm-1"></div>
															<div class="col-sm-10">
                                                                <%-- <span class="btn btn-danger help-block has-error" style="border: 1px solid #ccc;">${errorNameShow }</span> --%>
															</div>
														</div>														
														
														<div class="form-group">
															<div class="input-group">
																<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
															    <div class="col-sm-10">
																    <form:input type="password" path="passwd" class="form-control" id="passwd" placeholder="Enter Password"/> 
															    </div>
															</div>
															
															<div class="col-sm-1"></div>
															<div class="col-sm-10">
                                                                <%-- <span class="btn btn-danger help-block has-error" style="border: 1px solid #ccc;">${errorPassShow }</span> --%>
															</div>
														</div>

														<div class="row">
															<div class="col-sm-2">
															</div>
															<div class="col-sm-10">
																<input type="submit" class="btn btn-primary btn-sm" value="Login" />
																<a href="#" style="padding-left: 20px;">Forgot your password?</a>
															</div>
														</div>
                                                    </form:form>
                                                </div>
                                                
                                                <!-- Form Register -->
                                                <div class="tab-pane" id="Registration">
                                                    <form:form role="form" class="form-horizontal" action="checkRegister.html" method="post" modelAttribute="registerInfo">
														<div class="form-group">
															<div class="input-group">
																<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
																<div class="col-sm-10">
																	<form:input type="text" path="ruser" class="form-control" placeholder="User Name" />
																</div>
															</div>
														</div>
														<div class="form-group">
															<div class="input-group">
																<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
																<div class="col-sm-10">
																	<form:input type="email" path="remail" class="form-control" id="email" placeholder="Email" />
																</div>
															</div>
														</div>
														<div class="form-group">
															<div class="input-group">
																<span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
																<div class="col-sm-10">
																	<form:input type="tel" path="rphone" class="form-control" id="phone" placeholder="Phone" />
																</div>
															</div>    
														</div>
														<div class="form-group">
															<div class="input-group">
																<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
																<div class="col-sm-10">
																	<form:input type="password" path="rpasswd" class="form-control" id="passwd" placeholder="Password" />
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-sm-2">
															</div>
															<div class="col-sm-10">
																<button type="submit" class="btn btn-primary btn-sm">Register</button>
																<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
															</div>
														</div>												
                                                    </form:form>
                                                </div>
                                            </div>
                                            <div id="OR" class="hidden-xs">OR</div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="row text-center sign-with">
                                                <div class="col-sm-12">
                                                    <h3>Sign in with</h3>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="btn-group btn-group-justified">
                                                        <a href="#" class="btn btn-primary">Facebook</a>
                                                        <a href="#" class="btn btn-danger">Google</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Login Form End-->        

            <!--Logo & Banner-->
            <div class="row">
                <div class="col-sm-6 thumbnail" id="logo-img">
                    <a href><img src="img/hinh/logothaythe.png" width="80%"></a>
                </div>
                
                <div class="col-sm-6 thumbnail" id="logo-img">
                    <a href><img src="img/hinh/logoH2.png" width="100%"></a>
                </div>
            </div>
            
            <!--Gio hang-->
            <div class="row thumbnail" id="giohang">
                <div class="col-sm-3">
                    <a href><p>BỘ VEST JUYP CHO NỮ CÔNG SỞ</p></a>
                </div>
                <div class="col-sm-3">
                    <a href><p>VÁY ĐẦM DẠ HỘI CHO NỮ GIỚI</p></a>
                </div>
                <div class="col-sm-3">
                    <a href><p>TRANG PHỤC CÔNG SỞ CHO NỮ GIỚI</p></a>
                </div>
                <div class="col-sm-3">
                    <a href><img src="img/hinh/giohang.png"></a>
                </div>
            </div>
            
            <!--Start Show gallery-->
            <div class="row thumbnail" id="slide-gallery">
                <!-- Part 1 -->
                <div class="col-sm-12">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <!-- Dấu tròn nhỏ hiển thị trên slide-->
                        <ol class="carousel-indicators" id="indicators-bgcolor">
                            <li data-target="#carousel-example-generic" data-slide-to="0"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1" class="active"></li>
                        </ol>
                        
                        <!--                Danh sách các hình sẽ được hiển thị-->
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="img/hinh/logochinh2.jpg" title="First slide">
                                <!-- Comment picture-->
<!--
                                <div class="carousel-caption" id="carousel-color">
                                    <h3>First slide</h3>
                                    <p>Sản phẩm đang HOT.</p>
                                </div>
-->
                            </div>
                            
                            <div class="item">
                                <img src="img/hinh/logochinh3.jpg" title="Second slide">
                            </div>
                        
                        </div>
                        
                        <!--    Button previous image in slide  -->
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                        <!--    Button next image in slide   -->
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                    </div>
                    
                    <!--            Add Headline and content of slide in here-->
<!--
                    <div class="main-text hidden-xs">
                        <div class="col-sm-12 text-center">
                            <h1>Static Headline And Content</h1>
                            <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
                            <a class="btn btn-clear btn-sm btn-min-block" href="#">Login</a>
                            <a class="btn btn-clear btn-sm btn-min-block" href="#">Registration</a>
                        </div>
                    </div>
-->
                    <!--            Add Headline and content of slide above-->    
                </div>
            </div>
            <!-- End Show gallery-->
            <br>