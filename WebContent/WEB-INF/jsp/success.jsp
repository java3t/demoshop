<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<html lang="en">
    <head>
        <title>Fashion TV</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="refresh" content="3;url=home.html" />
        <link rel="stylesheet" href="static/css/bootstrap.min.css">
         <script src="static/js/bootstrap.min.js"></script>
    </head>

    <body>
        <div class="container">
        	<h1>${message}</h1>
        	<h3>Redirecting in 3 seconds ...</h3>
        </div>
    </body>
</html>  