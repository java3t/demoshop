/// Solution auto slide 3

var speed = 15;
var tab = document.getElementById("slideB");
var tab1 = document.getElementById("inslideB1");
var tab2 = document.getElementById("inslideB2");

tab2.innerHTML = tab1.innerHTML;
// Command above you can use command below similar mean Using Jquery
// $("#inslideB2").append($("#inslideB1").html());

function Marquee() {
    if (tab2.offsetWidth - tab.scrollLeft <= 0)
        tab.scrollLeft -= tab1.offsetWidth
    else {tab.scrollLeft++;}
}

var MyMar = setInterval(Marquee, speed);

tab.onmouseover = function() { 
    clearInterval(MyMar); 
};

tab.onmouseout = function() { 
    MyMar = setInterval(Marquee, speed);
};